/**
* 
* @file
*
* @brief  Stub implementation for archive plugins support
*
* @author vitamin.caig@gmail.com
*
**/

//library includes
#include <core/plugins/registrator.h>

namespace ZXTune
{
  void RegisterArchivePlugins(ArchivePluginsRegistrator& /*registrator*/)
  {
  }
}
