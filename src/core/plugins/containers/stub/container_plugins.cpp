/**
* 
* @file
*
* @brief  Stub implementation of container plugins support
*
* @author vitamin.caig@gmail.com
*
**/

//library includes
#include <core/plugins/registrator.h>

namespace ZXTune
{
  void RegisterContainerPlugins(ArchivePluginsRegistrator& /*registrator*/)
  {
  }
}
