<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>MultipleItemsContextMenu</name>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="35"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="45"/>
        <source>Crop</source>
        <translation>Обрезать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="55"/>
        <source>Group together</source>
        <translation>Сгруппировать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="65"/>
        <source>Remove duplicates in</source>
        <translation>Удалить дубликаты среди выбранных</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="75"/>
        <source>Delete unavailable in</source>
        <translation>Удалить недоступные среди выбранных</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="80"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="93"/>
        <source>Rip-offs in this items</source>
        <translation>Похожие среди выбранных</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="98"/>
        <source>Items with the same types</source>
        <translation>Тех же типов</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="103"/>
        <source>Found in this items</source>
        <translation>Искать среди выбранных</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="108"/>
        <source>Copy all paths</source>
        <translation>Скопировать все пути</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="118"/>
        <source>Show statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="128"/>
        <source>Export selected</source>
        <translation>Экспорт выбранных</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/multiple_items_contextmenu.ui" line="138"/>
        <source>Convert selected</source>
        <translation>Сконвертировать выбранные</translation>
    </message>
</context>
<context>
    <name>NoItemsContextMenu</name>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="19"/>
        <source>Remove all duplicates</source>
        <translation>Удалить все дубликаты</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="29"/>
        <source>Delete all unavailable</source>
        <translation>Удалить все недоступные</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="34"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="46"/>
        <source>All rip-offs</source>
        <translation>Все похожие</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="51"/>
        <source>All found items</source>
        <translation>Все найденные</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="61"/>
        <source>Show total statistic</source>
        <translation>Общая статистика</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/no_items_contextmenu.ui" line="71"/>
        <source>Export all</source>
        <translation>Экспортировать все</translation>
    </message>
</context>
<context>
    <name>Playlist::UI::ItemsContextMenu</name>
    <message numerus="yes">
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="87"/>
        <source>%n item(s)</source>
        <translation>
            <numerusform>%n модуль</numerusform>
            <numerusform>%n модуля</numerusform>
            <numerusform>%n модулей</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="167"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="175"/>
        <source>Total duration: %1</source>
        <translation>Общее время: %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="177"/>
        <source>%n diferent modules&apos; type(s)</source>
        <translation>
            <numerusform>%n тип модулей</numerusform>
            <numerusform>%n типа модулей</numerusform>
            <numerusform>%n типов модулей</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="173"/>
        <source>Total: %1</source>
        <translation>Всего: %1</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="174"/>
        <source>Invalid: %1</source>
        <translation>Недоступно: %1</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="176"/>
        <source>Total size: %1</source>
        <translation>Общий размер: %1</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="227"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="232"/>
        <source>Converted: %1&lt;br/&gt;Failed: %2</source>
        <translation>Сконвертировано: %1&lt;br/&gt;Неудачно: %2</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="248"/>
        <source>Failed to open &apos;%1&apos; for conversion</source>
        <translation>Ошибка доступа к &apos;%1&apos; при конверсии</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/playlist_contextmenu.cpp" line="253"/>
        <source>Failed to convert &apos;%1&apos;: %2</source>
        <translation>Ошибка конверсии &apos;%1&apos;: %2</translation>
    </message>
</context>
<context>
    <name>Playlist::UI::PropertiesDialog</name>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="133"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="134"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="135"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="138"/>
        <source>Off</source>
        <translation>Выкл</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="138"/>
        <source>On</source>
        <translation>Вкл</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="157"/>
        <source>Samples frequency</source>
        <translation>Частота семплов, Гц</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="165"/>
        <source>Chip type</source>
        <translation>Тип микросхемы</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="178"/>
        <source>Mono</source>
        <translation>Моно</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="179"/>
        <source>Layout</source>
        <translation>Раскладка каналов</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="155"/>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="189"/>
        <source>Interpolation</source>
        <translation>Интерполяция</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="147"/>
        <source>Clockrate, Hz</source>
        <translation>Тактовая частота, Гц</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="150"/>
        <source>Frame duration, uS</source>
        <translation>Длительность кадра, мкс</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="186"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="187"/>
        <source>Performance</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="188"/>
        <source>Quality</source>
        <translation>Качество</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/properties_dialog.cpp" line="218"/>
        <source>Reset value</source>
        <translation>Сбросить значение</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="14"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="26"/>
        <source>What to find</source>
        <translation>Что искать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="51"/>
        <source>Where to find</source>
        <translation>Где искать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="63"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="73"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="83"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="96"/>
        <source>How to find</source>
        <translation>Как искать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="108"/>
        <source>Match case</source>
        <translation>Учитывать регистр</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/search_dialog.ui" line="115"/>
        <source>Regular expression</source>
        <translation>Регулярные выражения</translation>
    </message>
</context>
<context>
    <name>SingleItemContextMenu</name>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="24"/>
        <source>Play</source>
        <translation>Воспроизвести</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="34"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="44"/>
        <source>Crop</source>
        <translation>Обрезать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="54"/>
        <source>Remove duplicates of</source>
        <translation>Удалить дубликаты</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="59"/>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="71"/>
        <source>Rip-offs of this item</source>
        <translation>Похожие</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="76"/>
        <source>Items with the same type</source>
        <translation>Этого же типа</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="81"/>
        <source>Copy full path</source>
        <translation>Скопировать путь</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="91"/>
        <source>Export to file</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="101"/>
        <source>Convert</source>
        <translation>Конвертировать</translation>
    </message>
    <message>
        <location filename="../../apps/zxtune-qt/playlist/ui/desktop/single_item_contextmenu.ui" line="111"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
</context>
</TS>
